package sample

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.Matchers.*
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import javax.el.ELProcessor

class Sample {

    private var proc: ELProcessor = ELProcessor()

    @Before
    fun setUp() {
        proc = ELProcessor()
    }

    @Test
    fun helloworld() {
        proc.setVariable("greeting", "'hello, world'")
        val result = proc.eval("greeting") as String

        assertThat(result, `is`("hello, world"))
    }

    @Test
    fun variable() {
        proc.setVariable("var", "'abc'")
        proc.setValue("val", "abc")

        val result = proc.eval("var == val") as Boolean

        assertThat(result, `is`(true))
    }

    @Test
    fun bean() {

        data class Bean(var foo: String, var bar: Int)

        val bean = Bean("abc", 123)
        proc.defineBean("var", bean)

        // Genericsじゃないだと...?
        val res = proc.getValue("var", Bean::class.java) as Bean
        assertThat(res.foo, `is`("abc"))
        assertThat(proc.getValue("var.bar", Int::class.java) as Int, `is`(123))
    }

    @Suppress("UNCHECKED_CAST")
    @Test
    fun collection() {

        proc.setVariable("foo", "[1, 2, 3]")
        proc.setVariable("bar", "{'a': 1, 'b': 2, 'c': 3}")
        proc.setVariable("buz", "{'a', 'b', 'c'}")

        // longなんだ
        val list = proc.getValue("foo", List::class.java) as List<Long>
        val map = proc.getValue("bar", Map::class.java) as Map<String, Long>
        val set = proc.getValue("buz", Set::class.java) as Set<String>

        assertThat(list, contains(1L, 2L, 3L))
        assertThat(map, hasEntry("a", 1L))
        assertThat(map, hasEntry("b", 2L))
        assertThat(map, hasEntry("c", 3L))
        assertThat(set, containsInAnyOrder("a", "b", "c"))
    }

    @Test
    fun lambda() {
        proc.setVariable("var", "[1, 2, 3, 4, 5, 6]")
        val res = proc.eval("""var.stream()
                                  .filter(n -> n % 2 == 0)
                                  .map(n -> n * 2)
                                  .sum()""") as Long

        assertThat(res, `is`(24L))
    }

}
